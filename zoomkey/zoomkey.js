Hooks.once('init', async function () {
    window.addEventListener('keydown', event =>{
        if (event.keyCode === 34 || (event.ctrlKey && event.keyCode == 189)) { // Page Down or -
            canvas.pan({ scale: 0.95 * canvas.stage.scale.x }); 
        } else if (event.keyCode == 33 || (event.ctrlKey && event.keyCode == 187)) { // Page Up or +
            canvas.pan({ scale: 1.05 * canvas.stage.scale.x });    
        } else if ((event.keyCode == 38 && event.metaKey) || (event.keyCode == 38 && event.ctrlKey)) {   //arrow up
            canvas.pan({ y: canvas.scene._viewPosition.y - 100 })    
        } else if ((event.keyCode == 40 && event.metaKey) || (event.keyCode == 40 && event.ctrlKey)) {   //arrow down
            canvas.pan({ y: canvas.scene._viewPosition.y + 100 })    
        } else if ((event.keyCode == 37 && event.metaKey) || (event.keyCode == 37 && event.ctrlKey)) {   //arrow left
            canvas.pan({ x: canvas.scene._viewPosition.x - 100 })    
        } else if ((event.keyCode == 39 && event.metaKey) || (event.keyCode == 39 && event.ctrlKey)) {   //arrow right
            canvas.pan({ x: canvas.scene._viewPosition.x + 100 })    
        }
    });
});